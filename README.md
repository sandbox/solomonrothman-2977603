# Entity Decorator D8 Module

Entity Decorator module adds a decorator class for entities. See [Decorator pattern Wikipedia](https://en.wikipedia.org/wiki/Decorator_pattern)
This base class (EntityDecorator) implements EntityInterface and be be extended by your custom entity decorators. This module is 
This module is intended for developers and provides no functionality on it's own.

## Main Use Case
You have want to use the decorator pattern to add business logic or custom functionality to an existing entity that is defined elsehwere (core entity or custom).

## Prior Work and Acknowledgements
This module is the Drupal 8 equivalent (Written from scratch for Drupal 8) of https://www.drupal.org/project/entity_decorator and is also influenced by this discussion where entity_decorator (Not the version here) was considered for Drupal 8 core - https://www.drupal.org/project/drupal/issues/2924796.

## Requirements

* Drupal 8

## Installation
Entity Decorator can be installed via the
[standard Drupal installation process](http://drupal.org/node/895232).

## Usage
In your own custom module, define a custom class that extends the EntityDecorator class.

```php
use Drupal\entity_decorator_d8;
use Drupal\Core\Entity\EntityStorageInterface;

class CustomEntityDecorator extends EntityDecorator{
    static public $entityType = 'CustomEntityType';
    static public $bundle = 'custom_bundle';
  
  public function businessLogic(){
    // Do something 
  }  
      /** EntityInterface Static Methods in the which need to be decorated with entity type inheritance */
      public static function load($id){
        $entityType = self::$entityType;
        $entityType::load($id);
      }
    
      public static function loadMultiple(array $ids = NULL) {
        $entityType = self::$entityType;
        $entityType::loadMultiple($ids);
      }
    
      public static function create(array $values = []){
        $entityType = self::$entityType;
        $entityType::create($values);
      }
    
      public static function preCreate(EntityStorageInterface $storage, array &$values) {
        $entityType = self::$entityType;
        $entityType::preCreate($storage,$values);
      }
    
      public static function preDelete(EntityStorageInterface $storage, array $entities) {
        $entityType = self::$entityType;
        $entityType::preDelete($storage, $entities);
      }
    
      public static function postDelete(EntityStorageInterface $storage, array $entities) {
        $entityType = self::$entityType;
        $entityType::postDelete($storage, $entities);
      }
    
      public static function postLoad(EntityStorageInterface $storage, array &$entities){
        $entityType = self::$entityType;
        $entityType::postLoad($storage, $entities);
      }
  
}
```


After you've defined your custom decorator class, you can instantiate it and pass it your custom entity. Then call the methods directly on it. In the example below "custom_entity" is the module that defines the entity being decorated. Custom decorator is the custom module that defines your custom entity decorator class.

```php
use Drupal\custom_entity\Entity;
use Drupal\custom_entity_decorator\CustomEntityDecorator;
$entity_type = 'EntityType';
$entity_id = 1; // Use real entity id here
$entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
$decorated_entity = new CustomEntityDecorator($entity);
$decorated_entity->save(); // Call entity methods directly on decorated entity
$decorated_entity->businessLogic('Hello World'); // Call custom methods
```
